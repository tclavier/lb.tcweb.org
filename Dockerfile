from registry.gitlab.com/tclavier/docker-nginx

add histoiresdalpaga.fr.conf /etc/nginx/sites-enabled/
add kaydara.fr.conf     /etc/nginx/sites-enabled/
add sonar.tcweb.org.conf     /etc/nginx/sites-enabled/
